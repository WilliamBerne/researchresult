﻿using System;

public class Product : System.Object
{

	public string productName;
	public DateTime expiryDate;
	public string price;
	public string[] sizes;

	public Product(string newName, DateTime newExpiryDate, string newPrice, string[] newSizes){
		productName = newName;
		expiryDate = newExpiryDate;
		price = newPrice;
		sizes = newSizes;
	}
}
