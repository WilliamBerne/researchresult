﻿using System;

public class BadProduct : Product {

	public string reasonForBadness;

	public BadProduct(string newName, DateTime newExpiryDate, string newPrice, string[] newSizes, string newReasonForBadness)
	: base(newName, newExpiryDate, newPrice, newSizes)
	{
		productName = newName;
		expiryDate = newExpiryDate;
		price = newPrice;
		sizes=newSizes;
		reasonForBadness = newReasonForBadness;
	}
}
