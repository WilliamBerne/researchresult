﻿
namespace GoodlyInnovations.OptiworX.Common
{
	public enum SpotKind : int
	{
		Aid = 0,
		Cog = 1,
		Ruler = 2,
		Fitting = 3,
		Peg = 4,
		Misc = 5
	}
}