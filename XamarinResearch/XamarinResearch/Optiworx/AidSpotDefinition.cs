﻿
namespace GoodlyInnovations.OptiworX.Common
{
	public class AidSpotDefinition : SpotDefinition
	{
		#region Properties
		public override SpotKind Kind { get { return SpotKind.Aid; } }
		#endregion



		#region Constructors
		public AidSpotDefinition()
		{
		}
		#endregion
	}
}