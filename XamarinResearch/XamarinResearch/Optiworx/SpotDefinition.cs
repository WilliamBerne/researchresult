﻿
namespace GoodlyInnovations.OptiworX.Common
{
	public abstract class SpotDefinition : Definition
	{
		#region Fields
		public abstract SpotKind Kind { get; }

		public string Id;

		public string Code;
		public string Title;
		#endregion



		#region Constructors
		public SpotDefinition()
		{
		}
		#endregion
	}
}