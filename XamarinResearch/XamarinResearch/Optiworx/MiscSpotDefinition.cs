﻿
namespace GoodlyInnovations.OptiworX.Common
{
	public class MiscSpotDefinition : SpotDefinition
	{
		#region Properties
		public override SpotKind Kind { get { return SpotKind.Misc; } }
		#endregion



		#region Constructors
		public MiscSpotDefinition()
		{
		}
		#endregion
	}
}