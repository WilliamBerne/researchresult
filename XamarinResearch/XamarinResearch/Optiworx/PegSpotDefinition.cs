﻿
namespace GoodlyInnovations.OptiworX.Common
{
	public class PegSpotDefinition : SpotDefinition
	{
		#region Properties
		public override SpotKind Kind { get { return SpotKind.Peg; } }
		#endregion



		#region Constructors
		public PegSpotDefinition()
		{
		}
		#endregion
	}
}