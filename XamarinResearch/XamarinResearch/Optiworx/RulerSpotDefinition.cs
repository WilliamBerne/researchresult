﻿
namespace GoodlyInnovations.OptiworX.Common
{
	public class RulerSpotDefinition : SpotDefinition
	{
		#region Properties
		public override SpotKind Kind { get { return SpotKind.Ruler; } }
		#endregion



		#region Constructors
		public RulerSpotDefinition()
		{
		}
		#endregion
	}
}