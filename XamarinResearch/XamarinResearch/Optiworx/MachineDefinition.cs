﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;



namespace GoodlyInnovations.OptiworX.Common
{
	public class MachineDefinition : Definition
	{
		#region Properties
		public Dictionary<string, SpotDefinition> SpotDefinitionsById;
		#endregion



		#region Constructors
		public MachineDefinition()
		{
			this.SpotDefinitionsById = new Dictionary<string, SpotDefinition>();
		}
		#endregion

	}
}