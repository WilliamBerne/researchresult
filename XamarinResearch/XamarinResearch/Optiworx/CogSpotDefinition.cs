﻿
namespace GoodlyInnovations.OptiworX.Common
{
	public class CogSpotDefinition : SpotDefinition
	{
		#region Properties
		public override SpotKind Kind { get { return SpotKind.Cog; } }
		#endregion



		#region Constructors
		public CogSpotDefinition()
		{
		}
		#endregion
	}
}