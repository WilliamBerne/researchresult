﻿

namespace GoodlyInnovations.OptiworX.Common
{
	public class FittingSpotDefinition : SpotDefinition
	{
		#region Properties
		public override SpotKind Kind { get { return SpotKind.Fitting; } }
		#endregion



		#region Constructors
		public FittingSpotDefinition()
		{
		}
		#endregion
	}
}