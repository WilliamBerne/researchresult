﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinResearch
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}


		#region Button Events
		void SerializationMethodName1(object sender, EventArgs e)
		{
            SerializationDebug(JsonNetTester.CreateExampleJsonString());

        }

		void SerializationMethodName2(object sender, EventArgs e)
		{
            SerializationDebug(JsonNetTester.CheckJsonString(this.serializationDebugText.Text));
        }


		void NetworkMethodName1(object sender, EventArgs e)
		{
			NetworkDebug("test 2 1");
		}

		void NetworkMethodName2(object sender, EventArgs e)
		{
			NetworkDebug("test 2 2");
		}
		#endregion


		#region Helpers
		public void SerializationDebug(string debugMessage)
		{
			this.serializationDebugText.Text = debugMessage;
		}

		public void NetworkDebug(string debugMessage)
		{
			this.networkDebugText.Text = debugMessage;
		}
		#endregion
	}
}
