﻿using System.IO;
using Newtonsoft.Json;

namespace Utils
{
    public class JsonSerializerUtils
    {
        #region (de)serialization
        public static string SerializeJson(System.Object objectToStore)
        {
            string jsonString = JsonConvert.SerializeObject(objectToStore, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, TypeNameHandling = TypeNameHandling.All });
            return jsonString;
        }

        public static byte[] SerializeJsonAsByteArray(System.Object objectToStore)
        {
            return ConvertToByteArray(SerializeJson(objectToStore));
        }

        public static System.Object DeserializeJson(string jsonString)
        {
            System.Object deserializedObject = JsonConvert.DeserializeObject<System.Object>(jsonString, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, TypeNameHandling = TypeNameHandling.All });
            return deserializedObject;
        }

        public static System.Object DeserializeJson(byte[] receivedBytes)
        {
            return DeserializeJson(ConvertToString(receivedBytes));
        }
        #endregion

        #region byte array conversion
        static byte[] ConvertToByteArray(string content)
        {
            return System.Text.Encoding.UTF8.GetBytes(content);
        }

        static string ConvertToString(byte[] data)
        {
            return System.Text.Encoding.UTF8.GetString(data);
        }
        #endregion

        #region fileHandling
        public static void WriteToJsonFile(string path, System.Object objectToWrite)
        {
            byte[] data = ConvertToByteArray(SerializeJson(objectToWrite));
            using (FileStream fileStream = new FileStream(path, FileMode.Create))
            {
                fileStream.Write(data, 0, data.Length);
            }
        }

        public static System.Object ReadFromJsonFile(string path)
        {
            byte[] data = System.IO.File.ReadAllBytes(path);
            string readString = ConvertToString(data);

            return DeserializeJson(readString);
        }
        #endregion
    }
}