﻿using GoodlyInnovations.OptiworX.Common;
using System;
using System.Collections.Generic;
using Utils;

public class JsonNetTester
{
    public static string CreateExampleJsonString()
    {
        return JsonSerializerUtils.SerializeJson(SetupExampleMachineDefinition());
    }

    public static string CheckJsonString(string jsonString)
    {
        Object deserializedObject = JsonSerializerUtils.DeserializeJson(jsonString);
        return ExampleMachineDefinitionInfo(deserializedObject);
    }



    private static MachineDefinition SetupExampleMachineDefinition()
    {
        MachineDefinition machineDefinition = new MachineDefinition();
        SpotDefinition cogSpotDefinitionOne = new CogSpotDefinition();
        SpotDefinition cogSpotDefinitionTwo = new CogSpotDefinition();
        SpotDefinition fittingSpotDefinition = new FittingSpotDefinition();
        SpotDefinition aidSpotDefinition = new AidSpotDefinition();
        Dictionary<string, SpotDefinition> spotDefinitionsById = new Dictionary<string, SpotDefinition>();
        spotDefinitionsById.Add("Cog Spot 1", cogSpotDefinitionOne);
        spotDefinitionsById.Add("Cog Spot 1 (copy)", cogSpotDefinitionOne);
        spotDefinitionsById.Add("Cog Spot 2", cogSpotDefinitionTwo);
        spotDefinitionsById.Add("Fitting Spot", fittingSpotDefinition);
        spotDefinitionsById.Add("Aid Spot", aidSpotDefinition);
        machineDefinition.SpotDefinitionsById = spotDefinitionsById;

        return machineDefinition;
    }

    private static string ExampleMachineDefinitionInfo(System.Object deserializedObject)
    {
        string objectInfo = "";
        MachineDefinition machineDefinition = (MachineDefinition) deserializedObject;
        SpotDefinition cogSpotDefinitionOne = machineDefinition.SpotDefinitionsById["Cog Spot 1"];
        SpotDefinition cogSpotDefinitionOneCopy = machineDefinition.SpotDefinitionsById["Cog Spot 1 (copy)"];
        SpotDefinition cogSpotDefinitionTwo = machineDefinition.SpotDefinitionsById["Cog Spot 2"];

        SpotDefinition fittingSpotDefinition = machineDefinition.SpotDefinitionsById["Fitting Spot"];
        SpotDefinition aidSpotDefinition = machineDefinition.SpotDefinitionsById["Aid Spot"];

        if (cogSpotDefinitionOne == cogSpotDefinitionOneCopy)
        {
            objectInfo = Append(objectInfo, "References to the same objects are recognized as such.");
        }
        else
        {
            objectInfo = Append(objectInfo, "[ERROR] References to the same objects are NOT recognized as such.");
        }

        if (cogSpotDefinitionOne != cogSpotDefinitionTwo)
        {
            objectInfo = Append(objectInfo, "References to identically set-up but distinct objects are recognized as such.");
        }
        else
        {
            objectInfo = Append(objectInfo, "[ERROR] References to identically set-up but distinct objects are NOT recognized as such.");
        }

        if (cogSpotDefinitionOne.GetType() == typeof(CogSpotDefinition) && aidSpotDefinition.GetType() == typeof(AidSpotDefinition) && fittingSpotDefinition.GetType() == typeof(FittingSpotDefinition))
        {
            objectInfo = Append(objectInfo, "Inheriting subclasses are recognized as such.");
        }
        else
        {
            objectInfo = Append(objectInfo, "[ERROR] Inheriting subclasses are NOT recognized as such.");
        }

        if (cogSpotDefinitionOne.Kind == SpotKind.Cog && aidSpotDefinition.Kind == SpotKind.Aid && fittingSpotDefinition.Kind == SpotKind.Fitting)
        {
            objectInfo = Append(objectInfo, "Inheriting subclasses do have respective kind enum.");
        }
        else
        {
            objectInfo = Append(objectInfo, "[ERROR] Inheriting subclasses do NOT have respective kind enum.");
        }

        return objectInfo;
    }

    //private static string ObjectInfo(System.Object deserializedObject)

    static string Append(string original, string newContent)
    {
        return original + newContent + "\n";
    }


    //private static ProductStorage SetupExampleProductStorage()
    //{
    //    ProductStorage productStorage = new ProductStorage();
    //    Product apple = new Product("Apple", new DateTime(2018, 12, 12), "3.99", new string[] { "Small", "Medium", "Large" });
    //    Product pear = new Product("Pear", new DateTime(2018, 11, 11), "5.99", new string[] { "Large", "Gigantic" });
    //    Product banana = new Product("Banana", new DateTime(2018, 10, 10), "0.99", new string[] { "Medium" });
    //    Product anotherBanana = new Product("Banana", new DateTime(2018, 10, 10), "0.99", new string[] { "Medium" });
    //    BadProduct poisonedApple = new BadProduct("Apple", new DateTime(2018, 10, 10), "0.99", new string[] { "Medium" }, "It's poisoned!");
    //    productStorage.companyName = "Super Fruit Company";
    //    Dictionary<string, Product[]> storeSupplies = productStorage.storeSupplies = new Dictionary<string, Product[]>();
    //    storeSupplies.Add("bananaStore", new Product[] { banana, banana, anotherBanana });
    //    storeSupplies.Add("appleStore", new Product[] { apple, apple, poisonedApple });
    //    storeSupplies.Add("generalFruitStore", new Product[] { apple, pear, pear, banana, poisonedApple });
    //    return productStorage;
    //}

    //private static string ProductStorageInfo(System.Object deserializedObject)
    //{
    //    string objectInfo = "";

    //    //objectInfo = Append(objectInfo, deserializedObject.ToString());
    //    ProductStorage deserializedProductStorage = (ProductStorage)deserializedObject;
    //    //objectInfo = Append(objectInfo, deserializedProductStorage.companyName);
    //    Product[] bananaStoreProducts = deserializedProductStorage.storeSupplies["bananaStore"];
    //    //objectInfo = Append(objectInfo, bananaStoreProducts[0].productName);
    //    //objectInfo = Append(objectInfo, bananaStoreProducts[1].productName);
    //    //objectInfo = Append(objectInfo, bananaStoreProducts[2].productName);
    //    Product bananaStoreFirstProduct = bananaStoreProducts[0];
    //    Product bananaStoreSecondProduct = bananaStoreProducts[1];
    //    Product bananaStoreThirdProduct = bananaStoreProducts[2];
        
    //    if (bananaStoreFirstProduct == bananaStoreSecondProduct)
    //    {
    //        objectInfo = Append(objectInfo, "First and second product are  correctly recognized as the same.");
    //    }
    //    else
    //    {
    //        objectInfo = Append(objectInfo, "[ERROR] First and second product are not recognized as the same, although they were generated from the same reference.");
    //    }

    //    if (bananaStoreFirstProduct != bananaStoreThirdProduct)
    //    {
    //        objectInfo = Append(objectInfo, "First and third product are correctly recognized as being not the same (even though they have the exact same values, they were not created with the same object reference).");
    //    }
    //    else
    //    {
    //        objectInfo = Append(objectInfo, "[ERROR] First and third product are not recognized as being different objects, although they are.");
    //    }

    //    Product[] appleStoreProducts = deserializedProductStorage.storeSupplies["appleStore"];
    //    if (appleStoreProducts[2].GetType() == typeof(BadProduct))
    //    {
    //        objectInfo = Append(objectInfo, "The inheriting subclass is correctly recognized as a subclass.");
    //    }
    //    else
    //    {
    //        objectInfo = Append(objectInfo, "[ERROR] The inheriting subclass is not recognized as the subclass.");
    //    }
    //    BadProduct badApple = (BadProduct)appleStoreProducts[2];
    //    if (badApple.reasonForBadness != "")
    //    {
    //        objectInfo = Append(objectInfo, "The inheriting subclass has an additional field, with content: " + badApple.reasonForBadness);
    //    }
    //    else
    //    {
    //        objectInfo = Append(objectInfo, "[ERROR] The inheriting subclass has an additional field, which was set for the object reference, but is lacking.");
    //    }

    //    return objectInfo;
    //}
}